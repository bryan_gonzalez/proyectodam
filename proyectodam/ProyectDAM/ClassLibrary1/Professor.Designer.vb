﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Professor
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.MenuItemCrearTarea = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuItemAssignarTarea = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuItemAssignarProyecto = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuItemVer = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProyectoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TareaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AlumnoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PanelUsuario = New System.Windows.Forms.Panel()
        Me.lblUser = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lblRol = New System.Windows.Forms.Label()
        Me.PanelMenu1 = New System.Windows.Forms.Panel()
        Me.btnBuscar = New System.Windows.Forms.Button()
        Me.txtBuscar = New System.Windows.Forms.TextBox()
        Me.lblAssigna = New System.Windows.Forms.Label()
        Me.lblTareas = New System.Windows.Forms.Label()
        Me.tabAlumnosProyectos = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.btnLogout = New System.Windows.Forms.Button()
        Me.MenuStrip1.SuspendLayout()
        Me.PanelUsuario.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.PanelMenu1.SuspendLayout()
        Me.tabAlumnosProyectos.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MenuItemCrearTarea, Me.MenuItemAssignarTarea, Me.MenuItemAssignarProyecto, Me.MenuItemVer})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(782, 28)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'MenuItemCrearTarea
        '
        Me.MenuItemCrearTarea.Name = "MenuItemCrearTarea"
        Me.MenuItemCrearTarea.Size = New System.Drawing.Size(97, 24)
        Me.MenuItemCrearTarea.Text = "Crear Tarea"
        '
        'MenuItemAssignarTarea
        '
        Me.MenuItemAssignarTarea.Name = "MenuItemAssignarTarea"
        Me.MenuItemAssignarTarea.Size = New System.Drawing.Size(118, 24)
        Me.MenuItemAssignarTarea.Text = "Assignar Tarea"
        '
        'MenuItemAssignarProyecto
        '
        Me.MenuItemAssignarProyecto.Name = "MenuItemAssignarProyecto"
        Me.MenuItemAssignarProyecto.Size = New System.Drawing.Size(139, 24)
        Me.MenuItemAssignarProyecto.Text = "Assignar Proyecto"
        '
        'MenuItemVer
        '
        Me.MenuItemVer.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ProyectoToolStripMenuItem, Me.TareaToolStripMenuItem, Me.AlumnoToolStripMenuItem})
        Me.MenuItemVer.Name = "MenuItemVer"
        Me.MenuItemVer.Size = New System.Drawing.Size(43, 24)
        Me.MenuItemVer.Text = "Ver"
        '
        'ProyectoToolStripMenuItem
        '
        Me.ProyectoToolStripMenuItem.Name = "ProyectoToolStripMenuItem"
        Me.ProyectoToolStripMenuItem.Size = New System.Drawing.Size(136, 24)
        Me.ProyectoToolStripMenuItem.Text = "Proyecto"
        '
        'TareaToolStripMenuItem
        '
        Me.TareaToolStripMenuItem.Name = "TareaToolStripMenuItem"
        Me.TareaToolStripMenuItem.Size = New System.Drawing.Size(136, 24)
        Me.TareaToolStripMenuItem.Text = "Tarea"
        '
        'AlumnoToolStripMenuItem
        '
        Me.AlumnoToolStripMenuItem.Name = "AlumnoToolStripMenuItem"
        Me.AlumnoToolStripMenuItem.Size = New System.Drawing.Size(136, 24)
        Me.AlumnoToolStripMenuItem.Text = "Alumno"
        '
        'PanelUsuario
        '
        Me.PanelUsuario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanelUsuario.Controls.Add(Me.lblUser)
        Me.PanelUsuario.Location = New System.Drawing.Point(0, 31)
        Me.PanelUsuario.Name = "PanelUsuario"
        Me.PanelUsuario.Size = New System.Drawing.Size(173, 26)
        Me.PanelUsuario.TabIndex = 1
        '
        'lblUser
        '
        Me.lblUser.AutoSize = True
        Me.lblUser.Location = New System.Drawing.Point(13, 4)
        Me.lblUser.Name = "lblUser"
        Me.lblUser.Size = New System.Drawing.Size(51, 17)
        Me.lblUser.TabIndex = 0
        Me.lblUser.Text = "Label1"
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.lblRol)
        Me.Panel1.Location = New System.Drawing.Point(0, 55)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(173, 27)
        Me.Panel1.TabIndex = 2
        '
        'lblRol
        '
        Me.lblRol.AutoSize = True
        Me.lblRol.Location = New System.Drawing.Point(11, 4)
        Me.lblRol.Name = "lblRol"
        Me.lblRol.Size = New System.Drawing.Size(51, 17)
        Me.lblRol.TabIndex = 0
        Me.lblRol.Text = "Label1"
        '
        'PanelMenu1
        '
        Me.PanelMenu1.Controls.Add(Me.btnBuscar)
        Me.PanelMenu1.Controls.Add(Me.txtBuscar)
        Me.PanelMenu1.Controls.Add(Me.lblAssigna)
        Me.PanelMenu1.Controls.Add(Me.lblTareas)
        Me.PanelMenu1.Location = New System.Drawing.Point(15, 88)
        Me.PanelMenu1.Name = "PanelMenu1"
        Me.PanelMenu1.Size = New System.Drawing.Size(275, 447)
        Me.PanelMenu1.TabIndex = 3
        '
        'btnBuscar
        '
        Me.btnBuscar.Location = New System.Drawing.Point(172, 405)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(75, 23)
        Me.btnBuscar.TabIndex = 4
        Me.btnBuscar.Text = "Buscar"
        Me.btnBuscar.UseVisualStyleBackColor = True
        '
        'txtBuscar
        '
        Me.txtBuscar.Location = New System.Drawing.Point(20, 405)
        Me.txtBuscar.Name = "txtBuscar"
        Me.txtBuscar.Size = New System.Drawing.Size(136, 22)
        Me.txtBuscar.TabIndex = 3
        '
        'lblAssigna
        '
        Me.lblAssigna.AutoSize = True
        Me.lblAssigna.Location = New System.Drawing.Point(17, 191)
        Me.lblAssigna.Name = "lblAssigna"
        Me.lblAssigna.Size = New System.Drawing.Size(58, 17)
        Me.lblAssigna.TabIndex = 1
        Me.lblAssigna.Text = "Assigna"
        '
        'lblTareas
        '
        Me.lblTareas.AutoSize = True
        Me.lblTareas.Location = New System.Drawing.Point(17, 33)
        Me.lblTareas.Name = "lblTareas"
        Me.lblTareas.Size = New System.Drawing.Size(53, 17)
        Me.lblTareas.TabIndex = 0
        Me.lblTareas.Text = "Tareas"
        '
        'tabAlumnosProyectos
        '
        Me.tabAlumnosProyectos.Controls.Add(Me.TabPage1)
        Me.tabAlumnosProyectos.Controls.Add(Me.TabPage2)
        Me.tabAlumnosProyectos.Location = New System.Drawing.Point(321, 88)
        Me.tabAlumnosProyectos.Name = "tabAlumnosProyectos"
        Me.tabAlumnosProyectos.SelectedIndex = 0
        Me.tabAlumnosProyectos.Size = New System.Drawing.Size(391, 447)
        Me.tabAlumnosProyectos.TabIndex = 4
        '
        'TabPage1
        '
        Me.TabPage1.AccessibleName = "L"
        Me.TabPage1.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.TabPage1.Location = New System.Drawing.Point(4, 25)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(383, 418)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Alumnos"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'TabPage2
        '
        Me.TabPage2.Location = New System.Drawing.Point(4, 25)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(383, 418)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Proyectos"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'btnLogout
        '
        Me.btnLogout.Location = New System.Drawing.Point(666, 47)
        Me.btnLogout.Name = "btnLogout"
        Me.btnLogout.Size = New System.Drawing.Size(82, 28)
        Me.btnLogout.TabIndex = 5
        Me.btnLogout.Text = "Log out"
        Me.btnLogout.UseVisualStyleBackColor = True
        '
        'Professor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(782, 553)
        Me.Controls.Add(Me.btnLogout)
        Me.Controls.Add(Me.tabAlumnosProyectos)
        Me.Controls.Add(Me.PanelMenu1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.PanelUsuario)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "Professor"
        Me.Text = "Form1"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.PanelUsuario.ResumeLayout(False)
        Me.PanelUsuario.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.PanelMenu1.ResumeLayout(False)
        Me.PanelMenu1.PerformLayout()
        Me.tabAlumnosProyectos.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents MenuItemCrearTarea As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuItemAssignarTarea As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuItemAssignarProyecto As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuItemVer As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProyectoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TareaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AlumnoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PanelUsuario As System.Windows.Forms.Panel
    Friend WithEvents lblUser As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lblRol As System.Windows.Forms.Label
    Friend WithEvents PanelMenu1 As System.Windows.Forms.Panel
    Friend WithEvents lblTareas As System.Windows.Forms.Label
    Friend WithEvents btnBuscar As System.Windows.Forms.Button
    Friend WithEvents txtBuscar As System.Windows.Forms.TextBox
    Friend WithEvents lblAssigna As System.Windows.Forms.Label
    Friend WithEvents tabAlumnosProyectos As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents btnLogout As System.Windows.Forms.Button
End Class
