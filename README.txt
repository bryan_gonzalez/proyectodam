Descargar el Repositorio en el ordenador

1. Crear un directorio permanente donde descargar el proyecto.
2. Instalar https://git-scm.com/download/win
3. Se trabajara sobre terminal de Git
4. Una vez instalado git damos click derecho sobre el escritorio, y damos clic en GIT Bash,	
	se abrira una terminal de Git.
Nos ubicamos en la Home, y creamos el siguiente directorio   -- mkdir .ssh
nos ubicamos dentro de ese directorio  --- cd .ssh/
y ejecutamos lo siguiente desde terminal   --- ssh-keygen -t rsa
nos generara 
Desde git tendremos que crear unas claves publicas y privadas.
Generamos las claves privadas y publicas desde terminal
con el siguiente codigo ---- ssh-keygen -t rsa -- 
luego nos pedira la direccion donde se generaran los shh, solo damos ENTER
y nos pedira una contrase�a (no olvidarla)
luego copiamos el contenido del fichero para ellos -- cat id_rsa.pub
nos dara una clave publica que la copiamos y la pegamos en el bitbucket

Configuramos el git con las cuentas y nombres de bitbucket

git config --global user.name "tu usuario de BitBucket aqu�"  -- y damos enter
git config --globla user.email "tu email de BitBucket aqu�"  -- y damos enter


5. Para movernos entre directorios en el ordenador se usa el comando "cd" sin comillas, y nos ubicaremos en 
	en el directorio del repositorio.
y desde la terminal pegamos git clone git@bitbucket.org:bryan_gonzalez/proyectodam.git  --- y damos ENTER -- 
nos pedira la clave de seguridad para descarga
esto hara una descarga del codigo actual que se encuentra en bitbucket.
6. luego se trabajara de forma individual creando una rama propia donde se haran los cambios necesarios

git status -- para ver en que rama estamos y lso archivos modificados
git checkout prod -- nos ubicamos en la rama principal
git checkout -b rama_nueva -- creamos una rama nueva
git checkout nombre_rama -- nos pasamos a una rama 
git pull -- descargamos las actualizaciones o modificaciones nuevas
git add nombre_archivo --- preparamos para subir un archivo
git commit -m 'mensajedeloscambiosrealizados' ---- le damos un nombre a las modificaciones realizadas
git push subimos al codigo principal lso cambios realizados


no hacer git push si los cambios no estan completos ya que si no dara problema a los demas cuando
quieran abrir el programa
