﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class InfoUser
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.MenuInfoUser = New System.Windows.Forms.MenuStrip()
        Me.menuItemUsuario = New System.Windows.Forms.ToolStripMenuItem()
        Me.menuItemCrear = New System.Windows.Forms.ToolStripMenuItem()
        Me.menuItemBuscar = New System.Windows.Forms.ToolStripMenuItem()
        Me.menuItemAgregar = New System.Windows.Forms.ToolStripMenuItem()
        Me.menuItemAyuda = New System.Windows.Forms.ToolStripMenuItem()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Editar = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtNom = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtApellido = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtDNI = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtTelefono = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cmbRol = New System.Windows.Forms.ComboBox()
        Me.MenuInfoUser.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuInfoUser
        '
        Me.MenuInfoUser.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.menuItemUsuario, Me.menuItemCrear, Me.menuItemBuscar, Me.menuItemAgregar, Me.menuItemAyuda})
        Me.MenuInfoUser.Location = New System.Drawing.Point(0, 0)
        Me.MenuInfoUser.Name = "MenuInfoUser"
        Me.MenuInfoUser.Size = New System.Drawing.Size(782, 28)
        Me.MenuInfoUser.TabIndex = 7
        Me.MenuInfoUser.Text = "MenuStrip1"
        '
        'menuItemUsuario
        '
        Me.menuItemUsuario.Name = "menuItemUsuario"
        Me.menuItemUsuario.Size = New System.Drawing.Size(71, 24)
        Me.menuItemUsuario.Text = "Usuario"
        '
        'menuItemCrear
        '
        Me.menuItemCrear.Name = "menuItemCrear"
        Me.menuItemCrear.Size = New System.Drawing.Size(56, 24)
        Me.menuItemCrear.Text = "Crear"
        '
        'menuItemBuscar
        '
        Me.menuItemBuscar.Name = "menuItemBuscar"
        Me.menuItemBuscar.Size = New System.Drawing.Size(64, 24)
        Me.menuItemBuscar.Text = "Buscar"
        '
        'menuItemAgregar
        '
        Me.menuItemAgregar.Name = "menuItemAgregar"
        Me.menuItemAgregar.Size = New System.Drawing.Size(75, 24)
        Me.menuItemAgregar.Text = "Agregar"
        '
        'menuItemAyuda
        '
        Me.menuItemAyuda.Name = "menuItemAyuda"
        Me.menuItemAyuda.Size = New System.Drawing.Size(63, 24)
        Me.menuItemAyuda.Text = "Ayuda"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(27, 62)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(134, 17)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Informacion Usuario"
        '
        'Editar
        '
        Me.Editar.Location = New System.Drawing.Point(609, 62)
        Me.Editar.Name = "Editar"
        Me.Editar.Size = New System.Drawing.Size(121, 23)
        Me.Editar.TabIndex = 9
        Me.Editar.Text = "Editar"
        Me.Editar.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.cmbRol)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.txtTelefono)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.txtDNI)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.txtApellido)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.txtNom)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Location = New System.Drawing.Point(71, 99)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(600, 403)
        Me.Panel1.TabIndex = 10
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(44, 24)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(120, 17)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Datos Personales"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(161, 112)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(58, 17)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Nombre"
        '
        'txtNom
        '
        Me.txtNom.Location = New System.Drawing.Point(278, 112)
        Me.txtNom.Name = "txtNom"
        Me.txtNom.Size = New System.Drawing.Size(176, 22)
        Me.txtNom.TabIndex = 2
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(164, 148)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(58, 17)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Apellido"
        '
        'txtApellido
        '
        Me.txtApellido.Location = New System.Drawing.Point(278, 148)
        Me.txtApellido.Name = "txtApellido"
        Me.txtApellido.Size = New System.Drawing.Size(176, 22)
        Me.txtApellido.TabIndex = 4
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(188, 183)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(31, 17)
        Me.Label5.TabIndex = 5
        Me.Label5.Text = "DNI"
        '
        'txtDNI
        '
        Me.txtDNI.Location = New System.Drawing.Point(278, 183)
        Me.txtDNI.Name = "txtDNI"
        Me.txtDNI.Size = New System.Drawing.Size(176, 22)
        Me.txtDNI.TabIndex = 6
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(170, 224)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(64, 17)
        Me.Label6.TabIndex = 7
        Me.Label6.Text = "Telefono"
        '
        'txtTelefono
        '
        Me.txtTelefono.Location = New System.Drawing.Point(278, 224)
        Me.txtTelefono.Name = "txtTelefono"
        Me.txtTelefono.Size = New System.Drawing.Size(176, 22)
        Me.txtTelefono.TabIndex = 8
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(182, 264)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(29, 17)
        Me.Label7.TabIndex = 9
        Me.Label7.Text = "Rol"
        '
        'cmbRol
        '
        Me.cmbRol.FormattingEnabled = True
        Me.cmbRol.Location = New System.Drawing.Point(278, 264)
        Me.cmbRol.Name = "cmbRol"
        Me.cmbRol.Size = New System.Drawing.Size(121, 24)
        Me.cmbRol.TabIndex = 10
        '
        'InfoUser
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(782, 553)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Editar)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.MenuInfoUser)
        Me.Name = "InfoUser"
        Me.Text = "InfoUser"
        Me.MenuInfoUser.ResumeLayout(False)
        Me.MenuInfoUser.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuInfoUser As System.Windows.Forms.MenuStrip
    Friend WithEvents menuItemUsuario As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents menuItemCrear As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents menuItemBuscar As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents menuItemAgregar As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents menuItemAyuda As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Editar As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtNom As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtApellido As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cmbRol As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtTelefono As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtDNI As System.Windows.Forms.TextBox
End Class
