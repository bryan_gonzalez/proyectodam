﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class NuevaTarea
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblNuevaTarea = New System.Windows.Forms.Label()
        Me.lblProyecto = New System.Windows.Forms.Label()
        Me.cmbProyecto = New System.Windows.Forms.ComboBox()
        Me.lblTarea = New System.Windows.Forms.Label()
        Me.txtTarea = New System.Windows.Forms.TextBox()
        Me.lblDescripcion = New System.Windows.Forms.Label()
        Me.txtDescripcion = New System.Windows.Forms.TextBox()
        Me.lblTareaInicio = New System.Windows.Forms.Label()
        Me.txtTareaInicio = New System.Windows.Forms.TextBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.lblTareaFin = New System.Windows.Forms.Label()
        Me.txtHoras = New System.Windows.Forms.TextBox()
        Me.lblHoras = New System.Windows.Forms.Label()
        Me.lblCost = New System.Windows.Forms.Label()
        Me.txtCost = New System.Windows.Forms.TextBox()
        Me.btnAcceptar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lblNuevaTarea
        '
        Me.lblNuevaTarea.AutoSize = True
        Me.lblNuevaTarea.Location = New System.Drawing.Point(175, 27)
        Me.lblNuevaTarea.Name = "lblNuevaTarea"
        Me.lblNuevaTarea.Size = New System.Drawing.Size(91, 17)
        Me.lblNuevaTarea.TabIndex = 0
        Me.lblNuevaTarea.Text = "Nueva Tarea"
        '
        'lblProyecto
        '
        Me.lblProyecto.AutoSize = True
        Me.lblProyecto.Location = New System.Drawing.Point(233, 60)
        Me.lblProyecto.Name = "lblProyecto"
        Me.lblProyecto.Size = New System.Drawing.Size(64, 17)
        Me.lblProyecto.TabIndex = 1
        Me.lblProyecto.Text = "Proyecto"
        '
        'cmbProyecto
        '
        Me.cmbProyecto.FormattingEnabled = True
        Me.cmbProyecto.Location = New System.Drawing.Point(304, 60)
        Me.cmbProyecto.Name = "cmbProyecto"
        Me.cmbProyecto.Size = New System.Drawing.Size(421, 24)
        Me.cmbProyecto.TabIndex = 2
        '
        'lblTarea
        '
        Me.lblTarea.AutoSize = True
        Me.lblTarea.Location = New System.Drawing.Point(233, 119)
        Me.lblTarea.Name = "lblTarea"
        Me.lblTarea.Size = New System.Drawing.Size(46, 17)
        Me.lblTarea.TabIndex = 3
        Me.lblTarea.Text = "Tarea"
        '
        'txtTarea
        '
        Me.txtTarea.Location = New System.Drawing.Point(304, 119)
        Me.txtTarea.Name = "txtTarea"
        Me.txtTarea.Size = New System.Drawing.Size(421, 22)
        Me.txtTarea.TabIndex = 4
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(197, 184)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(82, 17)
        Me.lblDescripcion.TabIndex = 5
        Me.lblDescripcion.Text = "Descripcion"
        '
        'txtDescripcion
        '
        Me.txtDescripcion.Location = New System.Drawing.Point(304, 184)
        Me.txtDescripcion.Multiline = True
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(421, 145)
        Me.txtDescripcion.TabIndex = 6
        '
        'lblTareaInicio
        '
        Me.lblTareaInicio.AutoSize = True
        Me.lblTareaInicio.Location = New System.Drawing.Point(200, 353)
        Me.lblTareaInicio.Name = "lblTareaInicio"
        Me.lblTareaInicio.Size = New System.Drawing.Size(82, 17)
        Me.lblTareaInicio.TabIndex = 7
        Me.lblTareaInicio.Text = "Tarea Inicio"
        '
        'txtTareaInicio
        '
        Me.txtTareaInicio.Location = New System.Drawing.Point(304, 353)
        Me.txtTareaInicio.Name = "txtTareaInicio"
        Me.txtTareaInicio.Size = New System.Drawing.Size(138, 22)
        Me.txtTareaInicio.TabIndex = 8
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(304, 407)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(138, 22)
        Me.TextBox1.TabIndex = 9
        '
        'lblTareaFin
        '
        Me.lblTareaFin.AutoSize = True
        Me.lblTareaFin.Location = New System.Drawing.Point(197, 407)
        Me.lblTareaFin.Name = "lblTareaFin"
        Me.lblTareaFin.Size = New System.Drawing.Size(69, 17)
        Me.lblTareaFin.TabIndex = 10
        Me.lblTareaFin.Text = "Tarea Fin"
        '
        'txtHoras
        '
        Me.txtHoras.Location = New System.Drawing.Point(304, 451)
        Me.txtHoras.Name = "txtHoras"
        Me.txtHoras.Size = New System.Drawing.Size(138, 22)
        Me.txtHoras.TabIndex = 11
        '
        'lblHoras
        '
        Me.lblHoras.AutoSize = True
        Me.lblHoras.Location = New System.Drawing.Point(203, 455)
        Me.lblHoras.Name = "lblHoras"
        Me.lblHoras.Size = New System.Drawing.Size(46, 17)
        Me.lblHoras.TabIndex = 12
        Me.lblHoras.Text = "Horas"
        '
        'lblCost
        '
        Me.lblCost.AutoSize = True
        Me.lblCost.Location = New System.Drawing.Point(203, 501)
        Me.lblCost.Name = "lblCost"
        Me.lblCost.Size = New System.Drawing.Size(36, 17)
        Me.lblCost.TabIndex = 14
        Me.lblCost.Text = "Cost"
        '
        'txtCost
        '
        Me.txtCost.Location = New System.Drawing.Point(304, 497)
        Me.txtCost.Name = "txtCost"
        Me.txtCost.Size = New System.Drawing.Size(138, 22)
        Me.txtCost.TabIndex = 13
        '
        'btnAcceptar
        '
        Me.btnAcceptar.Location = New System.Drawing.Point(178, 554)
        Me.btnAcceptar.Name = "btnAcceptar"
        Me.btnAcceptar.Size = New System.Drawing.Size(75, 23)
        Me.btnAcceptar.TabIndex = 15
        Me.btnAcceptar.Text = "Acceptar"
        Me.btnAcceptar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(558, 554)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 16
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'NuevaTarea
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(986, 589)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnAcceptar)
        Me.Controls.Add(Me.lblCost)
        Me.Controls.Add(Me.txtCost)
        Me.Controls.Add(Me.lblHoras)
        Me.Controls.Add(Me.txtHoras)
        Me.Controls.Add(Me.lblTareaFin)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.txtTareaInicio)
        Me.Controls.Add(Me.lblTareaInicio)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Controls.Add(Me.lblDescripcion)
        Me.Controls.Add(Me.txtTarea)
        Me.Controls.Add(Me.lblTarea)
        Me.Controls.Add(Me.cmbProyecto)
        Me.Controls.Add(Me.lblProyecto)
        Me.Controls.Add(Me.lblNuevaTarea)
        Me.Name = "NuevaTarea"
        Me.Text = "NuevaTarea"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblNuevaTarea As System.Windows.Forms.Label
    Friend WithEvents lblProyecto As System.Windows.Forms.Label
    Friend WithEvents cmbProyecto As System.Windows.Forms.ComboBox
    Friend WithEvents lblTarea As System.Windows.Forms.Label
    Friend WithEvents txtTarea As System.Windows.Forms.TextBox
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents txtDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents lblTareaInicio As System.Windows.Forms.Label
    Friend WithEvents txtTareaInicio As System.Windows.Forms.TextBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents lblTareaFin As System.Windows.Forms.Label
    Friend WithEvents txtHoras As System.Windows.Forms.TextBox
    Friend WithEvents lblHoras As System.Windows.Forms.Label
    Friend WithEvents lblCost As System.Windows.Forms.Label
    Friend WithEvents txtCost As System.Windows.Forms.TextBox
    Friend WithEvents btnAcceptar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
End Class
