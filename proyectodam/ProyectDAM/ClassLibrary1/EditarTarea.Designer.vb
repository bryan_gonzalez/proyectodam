﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class EditarTarea
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TabEditarTarea = New System.Windows.Forms.TabControl()
        Me.TabTarea = New System.Windows.Forms.TabPage()
        Me.TabAlumnos = New System.Windows.Forms.TabPage()
        Me.ListTareas = New System.Windows.Forms.ListBox()
        Me.btnEditar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.lstAlumnos = New System.Windows.Forms.ListBox()
        Me.TabEditarTarea.SuspendLayout()
        Me.TabTarea.SuspendLayout()
        Me.TabAlumnos.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabEditarTarea
        '
        Me.TabEditarTarea.Controls.Add(Me.TabTarea)
        Me.TabEditarTarea.Controls.Add(Me.TabAlumnos)
        Me.TabEditarTarea.Location = New System.Drawing.Point(25, 24)
        Me.TabEditarTarea.Name = "TabEditarTarea"
        Me.TabEditarTarea.SelectedIndex = 0
        Me.TabEditarTarea.Size = New System.Drawing.Size(745, 517)
        Me.TabEditarTarea.TabIndex = 0
        '
        'TabTarea
        '
        Me.TabTarea.Controls.Add(Me.btnEliminar)
        Me.TabTarea.Controls.Add(Me.btnEditar)
        Me.TabTarea.Controls.Add(Me.ListTareas)
        Me.TabTarea.Location = New System.Drawing.Point(4, 25)
        Me.TabTarea.Name = "TabTarea"
        Me.TabTarea.Padding = New System.Windows.Forms.Padding(3)
        Me.TabTarea.Size = New System.Drawing.Size(737, 488)
        Me.TabTarea.TabIndex = 0
        Me.TabTarea.Text = "Tarea"
        Me.TabTarea.UseVisualStyleBackColor = True
        '
        'TabAlumnos
        '
        Me.TabAlumnos.Controls.Add(Me.lstAlumnos)
        Me.TabAlumnos.Location = New System.Drawing.Point(4, 25)
        Me.TabAlumnos.Name = "TabAlumnos"
        Me.TabAlumnos.Padding = New System.Windows.Forms.Padding(3)
        Me.TabAlumnos.Size = New System.Drawing.Size(737, 488)
        Me.TabAlumnos.TabIndex = 1
        Me.TabAlumnos.Text = "Alumnos"
        Me.TabAlumnos.UseVisualStyleBackColor = True
        '
        'ListTareas
        '
        Me.ListTareas.FormattingEnabled = True
        Me.ListTareas.ItemHeight = 16
        Me.ListTareas.Location = New System.Drawing.Point(31, 20)
        Me.ListTareas.Name = "ListTareas"
        Me.ListTareas.Size = New System.Drawing.Size(678, 388)
        Me.ListTareas.TabIndex = 0
        '
        'btnEditar
        '
        Me.btnEditar.Location = New System.Drawing.Point(46, 437)
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(75, 23)
        Me.btnEditar.TabIndex = 1
        Me.btnEditar.Text = "Editar"
        Me.btnEditar.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(624, 437)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 2
        Me.btnEliminar.Text = "Eliminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'lstAlumnos
        '
        Me.lstAlumnos.FormattingEnabled = True
        Me.lstAlumnos.ItemHeight = 16
        Me.lstAlumnos.Location = New System.Drawing.Point(22, 18)
        Me.lstAlumnos.Name = "lstAlumnos"
        Me.lstAlumnos.Size = New System.Drawing.Size(692, 404)
        Me.lstAlumnos.TabIndex = 0
        '
        'EditarTarea
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(782, 553)
        Me.Controls.Add(Me.TabEditarTarea)
        Me.Name = "EditarTarea"
        Me.Text = "Editar Tarea"
        Me.TabEditarTarea.ResumeLayout(False)
        Me.TabTarea.ResumeLayout(False)
        Me.TabAlumnos.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TabEditarTarea As System.Windows.Forms.TabControl
    Friend WithEvents TabTarea As System.Windows.Forms.TabPage
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnEditar As System.Windows.Forms.Button
    Friend WithEvents ListTareas As System.Windows.Forms.ListBox
    Friend WithEvents TabAlumnos As System.Windows.Forms.TabPage
    Friend WithEvents lstAlumnos As System.Windows.Forms.ListBox
End Class
