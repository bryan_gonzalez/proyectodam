﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CambiarPassword
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtPasswordAnterior = New System.Windows.Forms.TextBox()
        Me.txtPasswordNuevo = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtConfPasswordNuevo = New System.Windows.Forms.TextBox()
        Me.MenuCambiarPassword = New System.Windows.Forms.MenuStrip()
        Me.menuItemUsuario = New System.Windows.Forms.ToolStripMenuItem()
        Me.menuItemCrear = New System.Windows.Forms.ToolStripMenuItem()
        Me.menuItemBuscar = New System.Windows.Forms.ToolStripMenuItem()
        Me.menuItemAgregar = New System.Windows.Forms.ToolStripMenuItem()
        Me.menuItemAyuda = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.MenuCambiarPassword.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(203, 236)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(123, 17)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Password Anterior"
        '
        'txtPasswordAnterior
        '
        Me.txtPasswordAnterior.Location = New System.Drawing.Point(348, 233)
        Me.txtPasswordAnterior.Name = "txtPasswordAnterior"
        Me.txtPasswordAnterior.Size = New System.Drawing.Size(191, 22)
        Me.txtPasswordAnterior.TabIndex = 1
        '
        'txtPasswordNuevo
        '
        Me.txtPasswordNuevo.Location = New System.Drawing.Point(348, 282)
        Me.txtPasswordNuevo.Name = "txtPasswordNuevo"
        Me.txtPasswordNuevo.Size = New System.Drawing.Size(191, 22)
        Me.txtPasswordNuevo.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(206, 286)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(114, 17)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Password Nuevo"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(141, 331)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(179, 17)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Confirmar Password Nuevo"
        '
        'txtConfPasswordNuevo
        '
        Me.txtConfPasswordNuevo.Location = New System.Drawing.Point(348, 331)
        Me.txtConfPasswordNuevo.Name = "txtConfPasswordNuevo"
        Me.txtConfPasswordNuevo.Size = New System.Drawing.Size(191, 22)
        Me.txtConfPasswordNuevo.TabIndex = 5
        '
        'MenuCambiarPassword
        '
        Me.MenuCambiarPassword.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.menuItemUsuario, Me.menuItemCrear, Me.menuItemBuscar, Me.menuItemAgregar, Me.menuItemAyuda})
        Me.MenuCambiarPassword.Location = New System.Drawing.Point(0, 0)
        Me.MenuCambiarPassword.Name = "MenuCambiarPassword"
        Me.MenuCambiarPassword.Size = New System.Drawing.Size(782, 28)
        Me.MenuCambiarPassword.TabIndex = 6
        Me.MenuCambiarPassword.Text = "MenuStrip1"
        '
        'menuItemUsuario
        '
        Me.menuItemUsuario.Name = "menuItemUsuario"
        Me.menuItemUsuario.Size = New System.Drawing.Size(71, 24)
        Me.menuItemUsuario.Text = "Usuario"
        '
        'menuItemCrear
        '
        Me.menuItemCrear.Name = "menuItemCrear"
        Me.menuItemCrear.Size = New System.Drawing.Size(56, 24)
        Me.menuItemCrear.Text = "Crear"
        '
        'menuItemBuscar
        '
        Me.menuItemBuscar.Name = "menuItemBuscar"
        Me.menuItemBuscar.Size = New System.Drawing.Size(64, 24)
        Me.menuItemBuscar.Text = "Buscar"
        '
        'menuItemAgregar
        '
        Me.menuItemAgregar.Name = "menuItemAgregar"
        Me.menuItemAgregar.Size = New System.Drawing.Size(75, 24)
        Me.menuItemAgregar.Text = "Agregar"
        '
        'menuItemAyuda
        '
        Me.menuItemAyuda.Name = "menuItemAyuda"
        Me.menuItemAyuda.Size = New System.Drawing.Size(63, 24)
        Me.menuItemAyuda.Text = "Ayuda"
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(144, 367)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 23)
        Me.btnAceptar.TabIndex = 7
        Me.btnAceptar.Text = "Acceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(474, 367)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 8
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'CambiarPassword
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(782, 553)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.txtConfPasswordNuevo)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtPasswordNuevo)
        Me.Controls.Add(Me.txtPasswordAnterior)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.MenuCambiarPassword)
        Me.MainMenuStrip = Me.MenuCambiarPassword
        Me.Name = "CambiarPassword"
        Me.Text = "Cambiar Password"
        Me.MenuCambiarPassword.ResumeLayout(False)
        Me.MenuCambiarPassword.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtPasswordAnterior As System.Windows.Forms.TextBox
    Friend WithEvents txtPasswordNuevo As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtConfPasswordNuevo As System.Windows.Forms.TextBox
    Friend WithEvents MenuCambiarPassword As System.Windows.Forms.MenuStrip
    Friend WithEvents menuItemUsuario As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents menuItemCrear As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents menuItemBuscar As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents menuItemAgregar As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents menuItemAyuda As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
End Class
