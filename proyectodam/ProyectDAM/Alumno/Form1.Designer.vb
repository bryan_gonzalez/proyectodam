﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Alumno
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Recursos = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.List = New System.Windows.Forms.GroupBox()
        Me.TreeView1 = New System.Windows.Forms.TreeView()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Proyecto = New System.Windows.Forms.Label()
        Me.Recursos.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        Me.List.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Recursos
        '
        Me.Recursos.Controls.Add(Me.TabPage1)
        Me.Recursos.Controls.Add(Me.TabPage2)
        Me.Recursos.Controls.Add(Me.TabPage3)
        Me.Recursos.Controls.Add(Me.TabPage4)
        Me.Recursos.Location = New System.Drawing.Point(12, 12)
        Me.Recursos.Name = "Recursos"
        Me.Recursos.SelectedIndex = 0
        Me.Recursos.Size = New System.Drawing.Size(647, 473)
        Me.Recursos.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(639, 447)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Recursos"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'TabPage2
        '
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(639, 447)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Tareas"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'TabPage3
        '
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(639, 447)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "com"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.GroupBox2)
        Me.TabPage4.Controls.Add(Me.List)
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Size = New System.Drawing.Size(639, 447)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Participantes"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'List
        '
        Me.List.Controls.Add(Me.Proyecto)
        Me.List.Controls.Add(Me.TextBox2)
        Me.List.Controls.Add(Me.TreeView1)
        Me.List.Location = New System.Drawing.Point(3, 3)
        Me.List.Name = "List"
        Me.List.Size = New System.Drawing.Size(618, 216)
        Me.List.TabIndex = 2
        Me.List.TabStop = False
        Me.List.Text = "Participantes"
        '
        'TreeView1
        '
        Me.TreeView1.Location = New System.Drawing.Point(15, 42)
        Me.TreeView1.Name = "TreeView1"
        Me.TreeView1.Size = New System.Drawing.Size(597, 168)
        Me.TreeView1.TabIndex = 0
        '
        'DataGridView1
        '
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(15, 15)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(588, 195)
        Me.DataGridView1.TabIndex = 4
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.DataGridView1)
        Me.GroupBox2.Location = New System.Drawing.Point(3, 225)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(618, 210)
        Me.GroupBox2.TabIndex = 4
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Datos Personales"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(76, 16)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(231, 20)
        Me.TextBox2.TabIndex = 1
        '
        'Proyecto
        '
        Me.Proyecto.AutoSize = True
        Me.Proyecto.Location = New System.Drawing.Point(12, 19)
        Me.Proyecto.Name = "Proyecto"
        Me.Proyecto.Size = New System.Drawing.Size(60, 13)
        Me.Proyecto.TabIndex = 2
        Me.Proyecto.Text = "Id proyecto"
        '
        'Alumno
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(671, 497)
        Me.Controls.Add(Me.Recursos)
        Me.Name = "Alumno"
        Me.Text = "Alumno"
        Me.Recursos.ResumeLayout(False)
        Me.TabPage4.ResumeLayout(False)
        Me.List.ResumeLayout(False)
        Me.List.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Recursos As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents List As System.Windows.Forms.GroupBox
    Friend WithEvents Proyecto As System.Windows.Forms.Label
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents TreeView1 As System.Windows.Forms.TreeView

End Class
